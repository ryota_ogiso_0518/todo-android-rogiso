package com.example.todo_app

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.websarva.wings.android.todo_app.RecyclerViewHolder
import kotlinx.android.synthetic.main.row.view.*

class RecyclerAdapter(private val todoItemClickListener: (Todo) -> Unit) :
    RecyclerView.Adapter<RecyclerViewHolder>() {

    var todoList: List<Todo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.row, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val todo = todoList[position]
        holder.itemView.text_todo_title.text = todo.title
        holder.itemView.setOnClickListener {
            todoItemClickListener.invoke(todo)
        }
    }

    override fun getItemCount(): Int {
        return todoList.size
    }
}
