package com.example.todo_app

import retrofit2.Call
import retrofit2.http.*

interface TodoApiRequest {
    @GET("todos")
    fun getTodos(): Call<TodosGetResponse>

    @POST("todos")
    fun createTodo(
        @Body todoRequestBody: TodoRequestBody
    ): Call<BaseResponse>

    @PUT("todos/{id}")
    fun updateTodo(
        @Path("id") id: Int,
        @Body todoRequestBody: TodoRequestBody
    ): Call<BaseResponse>

    @DELETE("todos/{id}")
    fun deleteTodo(
        @Path("id") id: Int
    ): Call<BaseResponse>
}