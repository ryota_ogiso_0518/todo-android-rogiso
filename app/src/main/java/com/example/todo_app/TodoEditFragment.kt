package com.example.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {

    private val args: TodoEditFragmentArgs by navArgs()
    private val displayFormat = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val todo = args.todo
        if (todo != null) {
            setUpTexts(todo)
        }
        setUpEditText()
        setUpRegisterButton(todo?.id)
        text_todo_date_content.setOnClickListener {
            showDatePickerDialog()
        }
    }

    private fun updateTitleCounterTextColor() {
        val color = if (edit_todo_title.length() > TITLE_MAX_LENGTH) Color.RED else Color.GRAY
        text_title_counter.setTextColor(color)
    }

    private fun updateDetailCounterTextColor() {
        val color = if (edit_todo_detail.length() > TITLE_MAX_LENGTH) Color.RED else Color.GRAY
        text_detail_counter.setTextColor(color)
    }

    private fun reloadRegisterButtonState() {
        val isTitleTextCounter =
            edit_todo_title.length() in 1..TITLE_MAX_LENGTH
        val isDetailTextCounter = edit_todo_detail.length() <= DETAIL_MAX_LENGTH
        button_register.isEnabled = isTitleTextCounter && isDetailTextCounter
    }

    private fun setUpEditText() {
        edit_todo_title.addTextChangedListener {
            text_title_counter.text = edit_todo_title.length().toString()
            updateTitleCounterTextColor()
            reloadRegisterButtonState()
        }

        edit_todo_detail.addTextChangedListener {
            text_detail_counter.text = edit_todo_detail.length().toString()
            updateDetailCounterTextColor()
            reloadRegisterButtonState()
        }
    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { _, year, month, day ->
                text_todo_date_content.text = ("$year/${month + 1}/$day")
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.button_date_delete)
        ) { _, _ ->
            text_todo_date_content.text = ""
        }
        datePickerDialog.show()
    }

    private fun setUpTexts(todo: Todo) {
        edit_todo_title.setText(todo.title)
        todo.detail?.let {
            edit_todo_detail.setText(it)
        }
        todo.date?.let {
            text_todo_date_content.text = displayFormat.format(it)
        }
        text_title_counter.text = edit_todo_title.length().toString()
        text_detail_counter.text = edit_todo_detail.length().toString()
    }

    private fun setUpRegisterButton(id: Int?) {
        button_register.text =
            getString(if (id == null) R.string.register_button else R.string.update_button)

        button_register.setOnClickListener {
            CoroutineScope(Default).launch {
                sendTodo(id)
            }
        }
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val request = TodoApiClient().apiRequest
            val response = if (id == null) {
                request.createTodo(makeBody()).execute()
            } else {
                request.updateTodo(id, makeBody()).execute()
            }
            if (response.isSuccessful) {
                withContext(Main) {
                    onSuccess(id != null)
                }
            } else {
                val body =
                    Gson().fromJson(response.errorBody()?.string(), BaseResponse::class.java)
                withContext(Main) {
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unexpected_error_message))
            }
        }
    }

    private fun makeBody(): TodoRequestBody {
        val title = edit_todo_title.text.toString()
        val detail = edit_todo_detail.text.toString()
        val format = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
        val formatToString = SimpleDateFormat(DATE_PATTERN_SERVER)
        val dateText = text_todo_date_content.text
        val date = if (dateText.isBlank()) {
            ""
        } else {
            val parseDate = format.parse(dateText.toString())
            formatToString.format(parseDate)
        }
        return TodoRequestBody(title, detail, date)
    }

    private fun onSuccess(isUpdateTodo: Boolean) {
        val receiveMessage =
            getString(if (isUpdateTodo) R.string.todo_update_message else R.string.todo_registered_message)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value =
            Event(receiveMessage)
        findNavController().popBackStack()
    }

    companion object {
        private const val TITLE_MAX_LENGTH = 100
        private const val DETAIL_MAX_LENGTH = 1000
        private const val DATE_PATTERN = "yyyy/M/d"
        private const val DATE_PATTERN_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSSz"
    }
}
